Configuration
=============

Now that we have done with the installation, it is time to commence configurations.

PostgreSQL
----------

#. Edit pg_hba.conf:

    .. code-block:: console

     # python manage.py migrate
     # cd /var/lib/pgsql/9.6/data
     # vi pg_hba.conf
     # change peer to trust
     # Add - host    all             all             0.0.0.0/0                md5

#. Edit postgresql.conf:

    .. code-block:: console

     # cd /var/lib/pgsql/9.6/data
     # vi postgresql.conf
     # edit listen_addresses = '*'
     # systemctl restart postgresql-9.6.service

#. Create a password for default user:

   See `PostgreSQL's createuser command <https://www.postgresql.org/docs/current/static/app-createuser.html>`_ for details.

   .. code-block:: console

    # psql -U postgres
    # \password
    # Enter “bright1008” twice

#. Create a PostgreSQL database

   See `PostgreSQL's createdb command <https://www.postgresql.org/docs/current/static/app-createdb.html>`_ for details.

   .. code-block:: console

    # CREATE DATABASE creodb;
    # CREATE DATABASE kamailio;
    # psql creodb < creodb.sql;


#. Prepare the database:

   .. code-block:: console

    $ python manage.py migrate

   .. warning::

       This command will need to be able to create database extensions. If you get an error related to the ``CREATE EXTENSION`` command please review the notes from the user creation step.

Kamailio
--------

This configuration file is an update of default Kamailio 4.0.x configuration file.

#. Create a .pgpass at the root directory:

     Input ``localhost:*:*:username:password`` in the .pgpass and save

#. We then need to add some configurations in the ``/etc/kamailio/kamctlrc`` file:

     * Your SIP domain

     ``SIP_DOMAIN=IP address of the server``

     * Database read/write user

     ``DBRWUSER="postgres"``

     * Password for database read/write user

     ``DBRWPW="password"``

     * Database read only user

     ``DBROUSER="username"``

     * Password for database read only user

     ``DBROPW="password"``

     * Database access host (from where is kamctl used)

     ``DBACCESSHOST=127.0.0.1``

#. Create kamailio tables:

    .. code-block:: console

     # /usr/local/sbin/kamdbctl create
     or
     # kamdbctl create

#. Configuring ``/etc/kamailio/kamailio.cfg``:

    On the top of kamailio.cfg file, add

    .. code-block:: console

     #!define WITH_MYSQL
     #!define WITH_AUTH
     #!define WITH_USRLOCDB
     #!define WITH_ASTERISK

    .. code-block:: console

     #!define DBURL "postgres://username:password@localhost/kamailio"
     #!define DBASTURL "postgres://username:password@localhost/creodb"

    .. note::
        Remember to change the IP Address of the bind ip


    .. code-block:: console

     asterisk.bindip = "xxxxxxxxxxx" desc "Asterisk IP Address"
     asterisk.bindport = "xxxx" desc "Asterisk Port"
     kamailio.bindip = "xxxxxxxxxxx" desc "Kamailio IP Address"
     kamailio.bindport = "xxxx" desc "Kamailio Port

    add ``loadmodule "db_postgres.so"``

#. Then, give permission:
    .. code-block::console
     # chown -R kamailio:kamailio /var/run/kamailio
     # chown kamailio:kamailio /etc/default/kamailio
     # chown -R kamailio:kamailio /etc/kamailio/
     # echo "d /run/kamailio 0750 kamailio kamailio" > /etc/tmpfiles.d/kamailio.conf

Asterisk
--------

#. Using database ``creodb`` create the following tables
    * create table sipusers

    .. code-block:: sql

        CREATE TABLE public.sipusers
        (
            id integer NOT NULL DEFAULT nextval('sipusers_id_seq'::regclass),
            name character varying(80) COLLATE pg_catalog."default" NOT NULL,
            ipaddr character varying(15) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            port integer NOT NULL DEFAULT 0,
            regseconds integer NOT NULL DEFAULT 0,
            defaultuser character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            fullcontact character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            regserver character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            useragent character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            lastms integer NOT NULL DEFAULT 0,
            host character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            type sip_type,
            context character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            permit character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            deny character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            secret character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            md5secret character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            remotesecret character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            transport tsport,
            dtmfmode dtmf,
            directmedia directmedia,
            nat nat,
            callgroup character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            pickupgroup character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            language character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            disallow character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            allow character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            insecure character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            trustrpid decision,
            progressinband progress,
            promiscredir decision,
            useclientcode decision,
            accountcode character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            setvar character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            callerid character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            amaflags character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            callcounter decision,
            busylevel integer NOT NULL DEFAULT 0,
            allowoverlap decision,
            allowsubscribe decision,
            videosupport decision,
            maxcallbitrate integer NOT NULL DEFAULT 0,
            rfc2833compensate decision,
            mailbox character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            "session-timers" s_timers,
            "session-expires" integer NOT NULL DEFAULT 0,
            "session-minse" integer NOT NULL DEFAULT 0,
            "session-refresher" s_rfresher,
            t38pt_usertpsource character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            regexten character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            fromdomain character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            fromuser character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            qualify character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            defaultip character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            rtptimeout integer NOT NULL DEFAULT 0,
            rtpholdtimeout integer NOT NULL DEFAULT 0,
            sendrpid decision,
            outboundproxy character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            callbackextension character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            timert1 integer NOT NULL DEFAULT 0,
            timerb integer NOT NULL DEFAULT 0,
            qualifyfreq integer NOT NULL DEFAULT 0,
            constantssrc decision,
            contactpermit character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            contactdeny character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            usereqphone decision,
            textsupport decision,
            faxdetect decision,
            buggymwi decision,
            auth character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            fullname character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            trunkname character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            cid_number character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            callingpres c_press,
            mohinterpret character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            mohsuggest character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            parkinglot character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            hasvoicemail decision,
            subscribemwi decision,
            vmexten character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            autoframing decision,
            rtpkeepalive integer NOT NULL DEFAULT 0,
            "call-limit" integer NOT NULL DEFAULT 0,
            g726nonstandard decision,
            ignoresdpversion decision,
            allowtransfer decision,
            dynamic decision,
            sippasswd character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying
        )

    .. code-block:: sql

        CREATE TABLE public.sipregs
        (
            id integer NOT NULL DEFAULT nextval('sipregs_id_seq'::regclass),
            name character varying(80) COLLATE pg_catalog."default" NOT NULL,
            fullcontact character varying(80) COLLATE pg_catalog."default" NOT NULL,
            ipaddr character varying(15) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            port integer NOT NULL DEFAULT 0,
            username character varying(80) COLLATE pg_catalog."default" NOT NULL,
            regserver character varying(15) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            regseconds integer NOT NULL DEFAULT 0,
            defaultuser character varying(80) COLLATE pg_catalog."default" NOT NULL,
            useragent character varying(15) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
            lastms integer NOT NULL DEFAULT 0,
            CONSTRAINT sipregs_name_key UNIQUE (name)

        )

#. UnixODBC Configuration
    Edit ``/etc/odbcinst.ini`` and add::

        [PostgreSQL]
        Description     = ODBC for PostgreSQL
        Driver          = /usr/local/lib/psqlodbca.so
        Setup           = /usr/lib64/libodbcpsqlS.so
        FileUsage       = 1

    Edit ``/etc/odbc.ini`` and add::

        [asterisk-connector]
        Description         = PostgreSQL connection to 'asterisk' database
        Driver              = PostgreSQL
        Database            = creodb
        Servername          = localhost
        UserName            = postgres
        Password            = xxxxxxxx
        Port                = 5432
        Protocol            = 8.1
        ReadOnly            = No
        RowVersioning       = No
        ShowSystemTables    = No
        ShowOidColumn       = No
        FakeOidIndex        = No
        ConnSettings        =


#. Asterisk UnixODBC Configuration
    Edit ``/etc/asterisk/res_odbc.conf`` and set::

        [creodb]
        enabled => yes
        dsn => asterisk-connector
        username => postgres
        password => xxxxxxxx
        pre-connect => yes

    Edit ``/etc/asterisk/extconfig.conf`` and set::

        [settings]
        sipusers => odbc,creodb,sipusers
        sippeers => odbc,creodb,sipusers
        sipregs => odbc,creodb,sipregs
        voicemail => odbc,creodb,voicemail
        queue_log => odbc,creodb,report_queue

#. Asterisk Configuration
    In case you need to cache the realtime users, then edit ``/etc/asterisk/sip.conf`` and set:

     .. code-block:: console

        rtcachefriends=yes

    .. note::

        Be sure you update the listen IP and port as well if Asterisk is running on the same system with Kamailio.

NGINX
-----

    Edit ``/etc/nginx/nginx.conf`` and set:

   .. code-block:: ruby

    server {
        listen       8000 default_server;
        listen       [::]:8000 default_server;
        server_name  xxx.xxx.xxx.xxx;
        location / {
	    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_pass http://0.0.0.0:8001;
        }
    location /stream {
            proxy_pass http://0.0.0.0:9001;
            proxy_http_version 1.1;
            proxy_read_timeout 86400;
            proxy_redirect     off;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Host $server_name;
    }
	location /static/ {
       	   root  /home/;
    }
    error_page 404 /404.html;
        location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }
    }

Gunicorn
--------

    Create ``/etc/systemd/system/gunicorn.service`` and set:

    .. code-block:: ruby

        [Unit]
        Description=gunicorn
        After=network.target
        [Service]
        PIDFile=/run/gunicorn/pid
        User=xxx
        Group=nginx
        WorkingDirectory=/home/anuri
        ExecStart=/usr/bin/gunicorn anuri.wsgi --bind 0.0.0.0:8001 --log-level error --log-file=- --workers 5 --preload
        ExecReload=/bin/kill -s HUP $MAINPID
        ExecStop=/bin/kill -s TERM $MAINPID
        Restart=on-abort
        PrivateTmp=true
        [Install]
        WantedBy=multi-user.target

    .. note::

        xxx is the system user.

Daphne
------

    Create ``/etc/systemd/system/daphne.service`` and set:

    .. code-block:: ruby

        [Unit]
        Description=daphne daemon
        After=network.target
        [Service]
        PIDFile=/run/daphne/pid
        User=xxx
        Group=nginx
        WorkingDirectory=/home/anuri
        ExecStart=/usr/bin/daphne -b 0.0.0.0 -p 9000  anuri.asgi:channel_layer
        ExecReload=/bin/kill -s HUP $MAINPID
        ExecStop=/bin/kill -s TERM $MAINPID
        Restart=on-abort
        PrivateTmp=true
        [Install]
        WantedBy=multi-user.target

Worker
------

Create ``/etc/systemd/system/worker.service`` and set:

    .. code-block:: ruby

        [Unit]
        Description=worker daemon
        After=network.target
        [Service]
        User=xxx
        Group=creo
        WorkingDirectory=/home/anuri
        ExecStart=/usr/bin/python /home/creo/anuri/manage.py runworker --threads 4
        ExecReload=/bin/kill -s HUP $MAINPID
        ExecStop=/bin/kill -s TERM $MAINPID
        Restart=always
        PrivateTmp=true
        [Install]
        WantedBy=multi-user.target

    .. note::

        Remember to put notifier in ``/etc/init.d/`` and run ``chmod +x notifier``
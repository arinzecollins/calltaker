Getting Started
===============

.. toctree::
   :maxdepth: 1

   gettingstarted/installation
   gettingstarted/configuration

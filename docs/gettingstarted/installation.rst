Installation
============

.. note::

   CallTaker is built to work on CentOS 7 linux distribution. Make sure that development tools are installed:

.. code-block:: console

    # sudo yum update
    # sudo yum group install "Development Tools"

Prerequisites
-------------

Before you are ready to run CallTaker you will need additional software installed on your computer.


Python 2.7
~~~~~~~~~~

CallTaker requires Python 2.7 or later. A compatible version comes preinstalled with most CentOS 7 Linux systems. If that is not the case consult your distribution for instructions on how to install python 2.7

PostgreSQL
~~~~~~~~~~

CallTaker needs PostgreSQL version 9.4 or above to work. Use the `PostgreSQL download page <https://www.postgresql.org/download/>`_ to get instructions for your distribution OR follow the steps below:

.. code-block:: console

    # rpm -ivh https://yum.postgresql.org/9.6/redhat/rhel-7.3-x86_64/pgdg-centos96-9.6-3.noarch.rpm
    # yum update
    # yum install postgresql96 postgresql96-server postgresql96-libs postgresql96-contrib postgresql96-devel
    # /usr/pgsql-9.6/bin/postgresql96-setup initdb
    # systemctl enable postgresql-9.6.service
    # systemctl start postgresql-9.6.service

DAHDI(Digium Asterisk Hardware Device Interface)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

DAHDI is a collection of open source drivers, for linux, that are used to interface with a variety of telephony related hardware. It consists of two parts.

   * The `DAHDI-Linux <http://git.asterisk.org/gitweb/?p=dahdi/linux.git;/>`_ project contains the individual board drivers for the supported hardware.
   * The `DAHDI-Tools <http://git.asterisk.org/gitweb/?p=dahdi/tools.git;/>`_ project contains an assortment of user space utilities that are used to setup and test the drivers.

.. code-block:: console

    # wget http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-2.8.0-rc4+2.8.0-rc4.tar.gz
    # tar -xvf dahdi-linux-complete-2.8.0-rc4+2.8.0-rc4.tar.gz
    # cd dahdi-linux-complete-2.8.0-rc4+2.8.0-rc4
    # make
    # make install
    # make config

Libpri
~~~~~~

LibPRI is a library that adds support for ISDN (PRI and BRI). The use of LibPRI is optional, but since it takes very little time to install, doesn’t interfere with anything, and will come in handy if you ever want to add cards to a system at a later point, we recommend that you install it now.

.. warning::

    Before you can build libpri, you'll need to Build and Install DAHDI.

.. code-block:: console

    # wget http://downloads.asterisk.org/pub/telephony/libpri/old/libpri-1.5.0.tar.gz
    # tar -xvf libpri-1.5.0.tar.gz
    # cd libpri-1.5.0
    # make
    # make install

Asterisk
~~~~~~~~

`Asterisk <https://www.asterisk.org/get-started/>`_ is an open source framework for building communications applications. To install asterisk, follow the following steps:

.. code-block:: console

    # wget http://downloads.asterisk.org/pub/telephony/certified-asterisk/releases/asterisk-certified-11.6-cert18.tar.gz
    # tar -xvf asterisk-certified-11.6-cert18.tar.gz
    # cd asterisk-certified-11.6-cert18
    # cp contrib/init.d/rc.redhat.asterisk /etc/init.d/asterisk
    # ./configure

.. note::

    During configuration, select ``format_mp3`` and ``res_odbc``.
    then,

.. code-block:: console

    # make
    # make install
    # make samples

Kamailio
~~~~~~~~

A typical use case is Kamailio as a SIP proxy router to scale Asterisk, by handling the user authentication and registration, letting one or a farm of Asterisks to deal with call handling (e.g., IVR, transconding, gatewaying, prepaid billing, a.s.o.).

With such architecture, several other benefits can be achieved quickly:

    * increase of security - Kamailio handling SIP signaling only, can absorb easier the flooding attacks, protecting Asterisk
    * transport layer gatewaying - Kamailio has mature implementations for UDP, TCP, TLS and SCTP, therefore you can use it in front of Asterisk to translate between these protocols
    * load balancing - you can use several instances of Asterisk, Kamailio can do load balancing among them
    * high availability - Kamailio can be configured to re-route the call if selected Asterisk box does not react in a given period of time, e.g., if one Asterisk is not responsive in 2 sec, sent the call to another Asterisk

.. code-block:: console

    # cd /etc/yum.repos.d/
    # wget http://download.opensuse.org/repositories/home:/kamailio:/v4.4.x-rpms/CentOS_7/home:kamailio:v4.4.x-rpms.repo
    # yum update
    # yum install -y kamailio kamailio-presence kamailio-ldap kamailio-postgres kamailio-debuginfo kamailio-xmpp kamailio-unixodbc kamailio-utils kamailio-tls kamailio-outbound kamailio-gzcompress
    # chkconfig kamailio on

Redis
~~~~~

`Redis <https://redis.io/>`_ is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker

.. code-block:: console

    # yum install redis
    # systemctl enable redis
    # systemctl start redis

Psqlodbc
~~~~~~~~

Psqlodbc is used for realtime connection between asterisk and postgresql database. To install psqlodbc, follow the following steps:

.. code-block:: console

    # wget http://ftp.postgresql.org/pub/odbc/versions/src/psqlodbc-09.06.0310.tar.gz
    # tar -zxvf psqlodbc-xx.xx.xxxx.tar.gz
    # cd psqlodbc-xx.xx.xxxx
    # ./configure
    # make
    # make install

.. note::

    Please take note of the location is psqlodbc because you would need it in the ``odbcinst.ini`` configuration.

To complete the prerequisites, install the following packages:

.. code-block:: console

    # yum install nginx
    # yum install python-devel libjpeg-devel zlib-devel
    # yum install postgresql-devel
    # yum install python-devel

Installation
------------

#. Clone the repository (or use your own fork):

   .. code-block:: console

    # git clone https://arinzecollins@bitbucket.org/arinzecollins/anuri.git


#. Enter the directory:

   .. code-block:: console

    $ cd anuri/


#. Install all dependencies:

   We strongly recommend `creating a virtual environment <https://docs.python.org/3/tutorial/venv.html>`_ before installing any Python packages.

   .. code-block:: console

    $ pip install -r requirements.txt


#. Set ``SECRET_KEY`` environment variable.

   We try to provide usable default values for all of the settings.
   We've decided not to provide a default for ``SECRET_KEY`` as we fear someone would inevitably ship a project with the default value left in code.

   .. code-block:: console

    $ export SECRET_KEY='<mysecretkey>'

   .. warning::

       Secret key should be a unique string only your team knows.
       Running code with a known ``SECRET_KEY`` defeats many of Django’s security protections, and can lead to privilege escalation and remote code execution vulnerabilities.
       Consult `Django's documentation <https://docs.djangoproject.com/en/1.11/ref/settings/#secret-key>`_ for details.
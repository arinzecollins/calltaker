CallTaker
=========

An enterprise call taking solution written in Python.

Contents:

.. toctree::
   :maxdepth: 2

   gettingstarted
   architecture
   guides


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
